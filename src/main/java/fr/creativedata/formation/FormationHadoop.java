package fr.creativedata.formation;

import avro.User;
import org.apache.avro.Schema;
import org.apache.avro.file.DataFileReader;
import org.apache.avro.file.DataFileWriter;
import org.apache.avro.generic.GenericData;
import org.apache.avro.generic.GenericDatumReader;
import org.apache.avro.generic.GenericDatumWriter;
import org.apache.avro.generic.GenericRecord;
import org.apache.avro.io.DatumReader;
import org.apache.avro.io.DatumWriter;
import org.apache.avro.specific.SpecificDatumWriter;
import org.apache.commons.lang.RandomStringUtils;
import org.apache.commons.lang.math.RandomUtils;
import org.apache.hadoop.fs.FileSystem;
import org.apache.hadoop.fs.Path;
import org.apache.parquet.avro.AvroParquetWriter;
import org.apache.parquet.hadoop.ParquetWriter;
import org.apache.parquet.hadoop.metadata.CompressionCodecName;

import java.io.File;

/**
 * Created by aurelien on 03/10/15.
 */
public class FormationHadoop  {


    public static void main(String[] args) throws Exception{

        String uri = "hdfs://37.187.79.74:8020";

        // Schema
        Schema.Parser parser = new Schema.Parser();
        Schema schema = parser.parse(FormationHadoop.class.getClassLoader().getResourceAsStream("avro/User.avsc"));


        // User1 Generic Record
        GenericRecord user1 = new GenericData.Record(schema);
        user1.put("firstname", "aurélien");
        user1.put("lastname", "vandel");
        user1.put("age", 28);
        user1.put("sexe", "M");
        user1.put("department",27);
        user1.put("hasLoyaltyCardNumber",false);

        // User2 Generic Record
        GenericRecord user2 = new GenericData.Record(schema);
        user2.put("firstname", "stéphanie");
        user2.put("lastname", "brion");
        user2.put("age",30);
        user2.put("sexe", "F");
        user2.put("department",76);
        user2.put("hasLoyaltyCardNumber",false);

        // User3 Specific Record
        User user3 = new User();
        user3.setFirstname("florian");
        user3.setLastname("ferreira");
        user3.setAge(27);
        user3.setSexe("F");
        user3.setDepartment(76);
        user3.setHasLoyaltyCardNumber(true);

        //User4 specific Record
        User user4 = User.newBuilder()
                .setFirstname("nadiath")
                .setLastname("waidi")
                .setAge(25)
                .setSexe("M")
                .setDepartment(76)
                .setHasLoyaltyCardNumber(false)
                .build();

        // Ecriture
        File usersFile = new File("users.avro");
        DatumWriter<GenericRecord> writer = new GenericDatumWriter<GenericRecord>(schema);
        DataFileWriter<GenericRecord> dataFileWriter = new DataFileWriter<GenericRecord>(writer);
        dataFileWriter.create(schema, usersFile);
        dataFileWriter.append(user1);
        dataFileWriter.append(user2);
        dataFileWriter.close();

        // Add Specific User
        DatumWriter<User> userDatumWriter = new SpecificDatumWriter<User>(User.class);
        DataFileWriter<User> dataFileWriter2 = new DataFileWriter<User>(userDatumWriter);
        dataFileWriter2.appendTo(usersFile);
        dataFileWriter2.append(user3);
        dataFileWriter2.append(user4);
        dataFileWriter2.close();

        // Lecture
        DatumReader<GenericRecord> datumReader = new GenericDatumReader<GenericRecord>(schema);
        DataFileReader<GenericRecord> dataFileReader = new DataFileReader<GenericRecord>(usersFile, datumReader);
        GenericRecord user = null;

        // Delete & Create Directories
        FileSystem fs = HdfsUtils.getFileSystemFromUri(uri);
        Path directory = HdfsUtils.createDirectory(fs, "/formation-BB");

        // Creating a file in HDFS
        Path filePath = HdfsUtils.createPath(fs, directory, "users.dat");

        // Write in parquet
        int blockSize = 256 * 1024 * 1024;
        int pageSize = 64 * 1024;

        ParquetWriter parquetWriter =  AvroParquetWriter.builder(filePath)
                .withCompressionCodec(CompressionCodecName.SNAPPY)
                .withSchema(schema).withPageSize(pageSize).withRowGroupSize(blockSize)
                .build();

        while (dataFileReader.hasNext()) {
            user = dataFileReader.next(user);
            System.out.println(user);
            parquetWriter.write(user);
        }

        char[] sexes = {'M', 'F'};

        // Un milliard de user
        for (int i = 0;i<100000000;i++){
            User userRandom= User.newBuilder()
                    .setFirstname(RandomStringUtils.random(8, true, false))
                    .setLastname(RandomStringUtils.random(8, true, false))
                    .setAge(RandomUtils.nextInt(100))
                    .setSexe(RandomStringUtils.random(1, sexes))
                    .setDepartment(RandomUtils.nextInt(95))
                    .setHasLoyaltyCardNumber(RandomUtils.nextBoolean())
                    .build();
            parquetWriter.write(userRandom);
        }

        parquetWriter.close();

        // Create parquet table
        String tableName = "users";
        String filePathInHdfs = filePath.toString();
        System.out.println(filePathInHdfs);
        ImpalaUtils.createParquetLikeParquetFile(tableName, filePathInHdfs);
        ImpalaUtils.loadDataFromFile(tableName,filePathInHdfs);

    }

}
