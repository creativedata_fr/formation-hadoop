package fr.creativedata.formation;

import java.io.IOException;
import java.sql.Connection;
import java.sql.DriverManager;
import java.sql.SQLException;
import java.sql.Statement;

/**
 * Created by aurelien on 03/10/15.
 */
public class ImpalaUtils {

    /**
     * Created by aurelien on 25/08/15.
     */

    private static final String CONNECTION_URL = "jdbc:hive2://176.31.240.183:21050/;auth=noSasl";
    private static final String JDBC_DRIVER_NAME = "org.apache.hive.jdbc.HiveDriver";

    private static Connection getConnection() {

        Connection con = null;
        try {
            Class.forName(JDBC_DRIVER_NAME);
            con = DriverManager.getConnection(CONNECTION_URL);
        }catch (Exception e) {
            e.printStackTrace();
        }
        return con;
    }

    public static void insertSelectDataInParquetTable(String tableName,String likeTable){

        String sqlStatement = "INSERT INTO " + tableName + " SELECT * FROM " + likeTable;

        System.out.println("\n=============================================");
        System.out.println("Using Connection URL: " + CONNECTION_URL);
        System.out.println("Running Query: " + sqlStatement);

        Connection con = getConnection();

        try{
            Statement stmt = con.createStatement();
            stmt.execute(sqlStatement);
            stmt.execute("REFRESH METADATA");
        }
        catch (SQLException e) {
            e.printStackTrace();
        } catch (Exception e) {
            e.printStackTrace();
        } finally {
            try {
                con.close();
            } catch (Exception e) {
                // swallow
            }
        }
    }

    public static void createParquetLikeParquetFile(String tableName, String likeParketFilePath) throws IOException {

        String deleteStatement = "DROP TABLE IF EXISTS " + tableName;
        String insertStatement = "CREATE TABLE " + tableName + " LIKE PARQUET '" + likeParketFilePath + "' STORED AS PARQUET";

        System.out.println("\n=============================================");
        System.out.println("Using Connection URL: " + CONNECTION_URL);
        System.out.println("Running Query: " + insertStatement);

        Connection con = getConnection();

        try{
            Statement stmt = con.createStatement();
            stmt.execute(deleteStatement);
            stmt.execute(insertStatement);
        }
        catch (SQLException e) {
            e.printStackTrace();
        } catch (Exception e) {
            e.printStackTrace();
        } finally {
            try {
                con.close();
            } catch (Exception e) {
                // swallow
            }
        }
    }

    public static void loadDataFromFile(String tableName, String likeParketFilePath) throws IOException {

        String loadDataStatement = "LOAD DATA INPATH '" + likeParketFilePath + "' INTO TABLE " + tableName;


        System.out.println("\n=============================================");
        System.out.println("Using Connection URL: " + CONNECTION_URL);
        System.out.println("Running Query: " + loadDataStatement);

        Connection con = getConnection();

        try{
            Statement stmt = con.createStatement();
            stmt.execute(loadDataStatement);
        }
        catch (SQLException e) {
            e.printStackTrace();
        } catch (Exception e) {
            e.printStackTrace();
        } finally {
            try {
                con.close();
            } catch (Exception e) {
                // swallow
            }
        }
    }


    public static void createParquetLikeTable(String tableName, String likeTable) throws IOException {

        String deleteStatement = "DROP TABLE IF EXISTS " + tableName;
        String insertStatement = "CREATE TABLE " + tableName + " LIKE " + likeTable + " STORED AS PARQUET";

        System.out.println("\n=============================================");
        System.out.println("Using Connection URL: " + CONNECTION_URL);
        System.out.println("Running Query: " + insertStatement);

        Connection con = getConnection();

        try{
            Statement stmt = con.createStatement();
            stmt.execute(deleteStatement);
            stmt.execute(insertStatement);
        }
        catch (SQLException e) {
            e.printStackTrace();
        } catch (Exception e) {
            e.printStackTrace();
        } finally {
            try {
                con.close();
            } catch (Exception e) {
                // swallow
            }
        }
    }

    }

